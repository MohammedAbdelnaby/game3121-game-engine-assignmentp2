﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Unity.Mathematics;

public class RotateCameraX : MonoBehaviour
{
    private float speed = 200;
    public GameObject player;

    // Update is called once per frame
    void Update()
    {
        if (Keyboard.current.dKey.IsActuated())
        {
            transform.Rotate(new float3(0,1,0), 1.0f * speed * Time.deltaTime);
        }
        if (Keyboard.current.aKey.IsActuated())
        {
            transform.Rotate(new float3(0, 1, 0), -1.0f * speed * Time.deltaTime);
        }

        transform.position = player.transform.position; // Move focal point with player

    }
}
